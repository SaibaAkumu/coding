<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post("/login", [AuthController::class,"login"]);
Route::get("/companies", [CompanyController::class,"companies"])
    ->middleware(\App\Http\Middleware\authorizes::class);
Route::get("/users", [UserController::class,"users"]);
Route::get("/countries", [CountryController::class,"lista"]);
Route::post("/register", [AuthController::class,"register"]);
