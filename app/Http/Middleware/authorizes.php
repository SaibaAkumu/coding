<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class authorizes
{
    public function handle(Request $request, Closure $next){
        $user = null;
        $token_arr = explode('Bearer ', $request->header('x-auth-token'));

        if (!$token_arr || !isset($token_arr[1]) || strlen($token_arr[1]) < 30) {
        return response()->json([
            'Non hai i permessi per vedere questa pagina'
        ]);
    }else if ($token = $token_arr[1]) {
        if (!$user = $this->getUserByToken($token)) {
            return response()->json([
                'Non hai i permessi per vedere questa pagina'
            ]);
        } else {
            $request->loggeUser = $user;
        }
    }
        return $next($request);
    }
    private function getUserByToken($token, $user_field = 'apiToken')
    {
        $user_id = (int)explode('!', $token)[0];

        if ($user_token = User::where('id', $user_id)->value($user_field)) {
            if (Hash::check($token, $user_token)) {
                return User::find($user_id);
            }
        }

        return false;
    }
}
