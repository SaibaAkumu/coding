<?php


namespace App\Http\Controllers;


use App\Models\Address;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiController
{
    function register(Request $request) {
        $message = "L'inserimento è stato eseguito con successo.";
        $data = $request->only(["nome", "cognome", "username", "email", "password", "confermaPassword", "citta", "via", "cap", "regione", "nomeAzienda", "descrizione"]); //query che preleva dalla richiesta solo i campi specificati

        //validazione dati del form
        $validator = Validator::make($data, [
            'nome'=> ["required", "max:20", "regex:/^[a-zA-Z][a-zA-Z\s]*$/"],
            'cognome'=> ["required", "max:20", "regex:/^[a-zA-Z][a-zA-Z\s]*$/"],
            'username'=> ["required", "max:20", "regex:/^[a-zA-Z][a-zA-Z\s]*$/"],
            'email'=> ["required", "email"],
            'password' => ["required", "between:6,20"],
            'confermaPassword' => ["required", "same:password"],
            'citta'=> ["required", "max:50", "regex:/^[a-zA-Z][a-zA-Z\s]*$/"],
            'via'=> ["required", "max:100", "regex:/^[a-zA-Z][a-zA-Z\s]*$/"],
            'cap'=> ["required", "min:5", "regex:/^[0-9]{5}$/"],
            'regione'=> ["required"],
            'nomeAzienda'=>["nullable", "max:30", "regex:/^[a-zA-Z][a-zA-Z0-9\s]*$/"],
            'descrizione'=>["nullable", "max:500"],
        ]);
        if($validator->fails()) {
            return [
                'status' => false,
                'error' => $validator->errors(),
            ];
        }

        $company = null;
        if($request->nomeAzienda) {
            $company = new Company();
            $company->nome = $request->nomeAzienda;
            $company->descrizione = $request->descrizione;
            $company->save();
        }

        $user = new User();                         //inserimento nella tabella
        $user->nome = $request->nome;
        $user->cognome = $request->cognome;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        if($company != null)
            $user->companies_id = $company->id;

        $user->save();

        $address = new Address();
        $address->citta = $request->citta;
        $address->via = $request->via;
        $address->cap = $request->cap;
        $address->regione = $request->regione;
        $address->user_id = $user->id;
        $address->save();

        return [
            'status' => true,
            'message' => $message,
        ];
    }
    //funzione per il login
    function login(Request $request){

        $data = $request->only(['email', 'password']);
        if(Auth::attempt($data)){
            //utente loggato

            $user = User::query()->where('email', $request->get('email'))->first();
            $token = $this->generateToken($user->id);

            return [
                'user' => $user,
                'token'=> $token,
                'status' => true,
            ];
        }
        else{
            //utente non loggato
            return [
                'user' => null,
                'token'=> null,
                'status' => false,
            ];
        }
    }
}
