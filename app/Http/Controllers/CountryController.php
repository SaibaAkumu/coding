<?php


namespace App\Http\Controllers;


use App\Models\Country;

class CountryController extends Controller{

    function lista(){
        return Country::query()->select('*')->orderBy('nome')->get();
    }
}
