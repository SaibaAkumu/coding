<?php


namespace App\Http\Controllers;


use App\Models\Company;

class CompanyController extends Controller
{

    function companies(){
        return Company::query()->select('*')->orderBy('nome')->with([
            'users' => function($query){
                $query->orderBy('nome');
            },
            'users.address'
        ])->get();
    }
}
