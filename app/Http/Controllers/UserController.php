<?php


namespace App\Http\Controllers;


use App\Models\User;

class UserController extends Controller{

    function users(){
        return User::query()->select('*')->orderBy('cognome')->get();
    }
}
