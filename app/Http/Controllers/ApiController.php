<?php


namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller {

    private function genRandToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max - 1)];
        }
        return $token;
    }

    protected function generateToken($user_id, $user_field = 'apiToken', $length = 256){

        $user = User::query()->find($user_id);
        $token = $user_id . '!' . $this->genRandToken($length);
        $hash_token = Hash::make($token);
        $user->$user_field = $hash_token;
        $user->save();
        return $token;
    }
}
