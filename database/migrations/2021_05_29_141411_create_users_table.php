<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string("nome");
            $table->string("cognome");
            $table->string("username", 15);
            $table->string("email", 191)->unique();
            $table->string("password", 191);
            $table->string("apiToken")->nullable();
            $table->boolean("flag")->default("0");
            $table->foreignId("companies_id")->nullable()->references("id")
                ->on("companies")
                ->onDelete("cascade")
                ->onUpdate("no action");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
