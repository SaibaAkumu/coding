<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Company extends Seeder
{
    public $companies = [];
    public function run()
    {
        DB::table("companies")->insert($this->companies);
    }
    public function __construct()
    {
        $this->companies = [
            [
                "id" => 1,
                "nome"=> "coleCorporation",
            ],
            [
                "id" => 2,
                "nome"=> "iGennarini",
            ],
        ];
    }
}
