<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call("Database\Seeders\Company");
        $this->call("Database\Seeders\User");
        $this->call("Database\Seeders\Country");
        $this->call("Database\Seeders\Address");
    }
}
