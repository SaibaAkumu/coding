<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Country extends Seeder
{
    public $country = [
        [
            "nome"=> "Valle D'aosta",
        ],
        [
            "nome"=> "Trentino Alto Adige",
        ],
        [
            "nome"=> "Friuli Venezia Giulia",
        ],
        [
            "nome"=> "Piemonte",
        ],
        [
            "nome"=> "Lombardia",
        ],
        [
            "nome"=> "Veneto",
        ],
        [
            "nome"=> "Liguria",
        ],
        [
            "nome"=> "Emilia Romagna",
        ],
        [
            "nome"=> "Toscana",
        ],
        [
            "nome"=> "Umbria",
        ],
        [
            "nome"=> "Marche",
        ],
        [
            "nome"=> "Lazio",
        ],
        [
            "nome"=> "Abruzzo",
        ],
        [
            "nome"=> "Campania",
        ],
        [
            "nome"=> "Molise",
        ],
        [
            "nome"=> "Basilicata",
        ],
        [
            "nome"=> "Puglia",
        ],
        [
            "nome"=> "Calabria",
        ],
        [
            "nome"=> "Sicilia",
        ],
        [
            "nome"=> "Sardegna",
        ],
    ];
    public function run()
    {
        //inserimento dati nel db
        DB::table("countries")->insert($this->country);
    }
}
