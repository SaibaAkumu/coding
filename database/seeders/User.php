<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Seeder
{
    public $user = [];
    public function run()
    {
        DB::table("users")->insert($this->user);
    }
    public function __construct()
    {
       $this->user = [
            [
                "id" => 1,
                "nome"=> "Marco",
                "cognome"=> "Colella",
                "username"=> "Cole",
                "email"=> "marco.mc@gmail.com",
                "password" => Hash::make("colee.ok"),
                "flag" => 1,
                "companies_id" => 1,
            ],
            [
                "id" => 2,
                "nome"=> "Gennaro",
                "cognome"=> "Giangetti",
                "username"=> "capitanTostoni",
                "email"=> "gennaro.gg@ciao.it",
                "password" => Hash::make("giangetti.fraschetti"),
                "flag" => 1,
                "companies_id" => 2,
            ],
        ];
    }
}
