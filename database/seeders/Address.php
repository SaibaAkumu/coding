<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Address extends Seeder
{
    public $address = [];
    public function run()
    {
        DB::table("addresses")->insert($this->address);
    }
    public function __construct()
    {
        $this->address = [
            [
                "id" => 1,
                "citta"=> "Milano",
                "via"=> "Faustini",
                "cap"=> "20031",
                "regione"=> "Lombardia",
                "user_id"=> 1,
            ],
            [
                "id" => 2,
                "citta"=> "Aosta",
                "via"=> "pieroangela",
                "cap"=> "31245",
                "regione"=> "Valle D'aosta",
                "user_id"=> 2,
            ],
        ];
    }

}
